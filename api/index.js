const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const cors = require('cors');

const app = express();
const port = 8000;
app.use(express.json());
app.use(cors());

app.get('/:name', (req, res) => {
    res.send(req.params.name);
});

app.get('/encode/:name', (req, res) => {
    res.send(Vigenere.Cipher('password').crypt(req.params.name));
});

app.get('/decode/:name', (req, res) => {
    res.send(Vigenere.Decipher('password').crypt(req.params.name));
});

app.post('/encode', (req, res) => {
    res.send(vigenereFunction(req, res, 'Cipher'));
});

app.post('/decode', (req, res) => {
    res.send(vigenereFunction(req, res, 'Decipher'));
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});

const vigenereFunction = (req, res, cipher) => {
    const message = req.body.message;
    const passWord = req.body.password;

    if (!message || !passWord) {
        res.status(404).send('bad request');
    }

    if (cipher === 'Cipher') {
        const encode = Vigenere.Cipher(passWord).crypt(message);
        return ({
            encoded: encode
        });
    } else {
        const decode = Vigenere.Decipher(passWord).crypt(message);
        return ({
            decoded: decode
        });
    }

};


