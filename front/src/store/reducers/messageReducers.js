import {
    FETCH_DECODED_FAILURE,
    FETCH_DECODED_REQUEST, FETCH_DECODED_SUCCESS,
    FETCH_ENCODED_FAILURE,
    FETCH_ENCODED_REQUEST,
    FETCH_ENCODED_SUCCESS
} from "../actions/messageActions";


const initialState = {
    loading: false,
    error: null,
    message: null,
    encodedMessage: null,
    decodedMessage: null,
    password: null
};

const messageReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ENCODED_REQUEST:
            return {...state, loading: true};
        case FETCH_ENCODED_SUCCESS:
            return {...state, loading: false, encodedMessage: action.payload};
        case FETCH_ENCODED_FAILURE:
            return {...state, loading: false, error: action.payload};

        case FETCH_DECODED_REQUEST:
            return {...state, loading: true};
        case FETCH_DECODED_SUCCESS:
            return {...state, loading: false, decodedMessage: action.payload};
        case FETCH_DECODED_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
};

export default messageReducer;