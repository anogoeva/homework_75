import axios from "axios";

export const FETCH_ENCODED_REQUEST = 'FETCH_ENCODED_REQUEST';
export const FETCH_ENCODED_SUCCESS = 'FETCH_ENCODED_SUCCESS';
export const FETCH_ENCODED_FAILURE = 'FETCH_ENCODED_FAILURE';

export const FETCH_DECODED_REQUEST = 'FETCH_DECODED_REQUEST';
export const FETCH_DECODED_SUCCESS = 'FETCH_DECODED_SUCCESS';
export const FETCH_DECODED_FAILURE = 'FETCH_DECODED_FAILURE';


export const fetchEncodedRequest = message => ({type: FETCH_ENCODED_REQUEST});
export const fetchEncodedSuccess = message => ({type: FETCH_ENCODED_SUCCESS, payload: message});
export const fetchEncodedFailure = message => ({type: FETCH_ENCODED_FAILURE});


export const fetchDecodedRequest = message => ({type: FETCH_DECODED_REQUEST});
export const fetchDecodedSuccess = message => ({type: FETCH_DECODED_SUCCESS, payload: message});
export const fetchDecodedFailure = message => ({type: FETCH_DECODED_FAILURE});


export const fetchEncodeMessage = message => {
    return async dispatch => {
        try {
            dispatch(fetchEncodedRequest());
            const response = await axios.post('http://localhost:8000/encode/', message);
            dispatch(fetchEncodedSuccess(response.data));
        } catch (error) {
            dispatch(fetchEncodedFailure(error));
        }
    }
};
export const fetchDecodeMessage = message => {
    return async dispatch => {
        try {
            dispatch(fetchDecodedRequest());
            const response = await axios.post('http://localhost:8000/decode/', message);
            dispatch(fetchDecodedSuccess(response.data));
        } catch (error) {
            dispatch(fetchDecodedFailure(error));
        }
    }
};