import React, {useState} from 'react';
import {Grid, IconButton, TextField} from "@material-ui/core";
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {fetchDecodeMessage, fetchEncodeMessage} from "../../store/actions/messageActions";
import Spinner from "../../Components/Spinner/Spinner";

const MainPage = () => {
    const dispatch = useDispatch();
    const encodedMessage = useSelector(state => state.message.encodedMessage);
    const decodedMessage = useSelector(state => state.message.decodedMessage);
    const loading = useSelector(state => state.message.loading);

    const [inputBody, setInputBody] = useState({
        encode: '',
        password: '',
        decode: ''
    });

    const onInputChange = e => {
        const {name, value} = e.target;
        setInputBody(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const encodeMessage = () => {
        if (inputBody.password === '') {
            alert('Введите пароль')
        } else {
            const obj = {
                password: inputBody.password,
                message: inputBody.decode
            }
            dispatch(fetchEncodeMessage(obj));
        }
    };

    const decodeMessage = () => {
        if (inputBody.password === '') {
            alert('Введите пароль')
        } else {
            const obj = {
                password: inputBody.password,
                message: inputBody.encode
            }
            dispatch(fetchDecodeMessage(obj));
        }
    };

    let form = (
        <Grid container direction="column" spacing={2}>
            <Grid item xs>
                <TextField
                    fullWidth
                    variant="outlined"
                    label="decode"
                    value={decodedMessage ? decodedMessage.decoded : inputBody.decode}
                    onChange={onInputChange}
                    name="decode"
                />
            </Grid>

            <Grid item xs>
                <TextField id="outlined-basic" label="password"
                           required
                           variant="outlined"
                           onChange={onInputChange}
                           name="password"
                           type="password"
                />
            </Grid>

            <Grid item xs>
                <IconButton component={Link} to="" onClick={decodeMessage}>
                    <ArrowUpwardIcon/>
                </IconButton>

            </Grid>

            <Grid item xs>
                <IconButton component={Link} to="" onClick={encodeMessage}>
                    <ArrowDownwardIcon/>
                </IconButton>
            </Grid>

            <Grid item xs>
                <TextField
                    fullWidth
                    variant="outlined"
                    label="encode"
                    value={encodedMessage ? encodedMessage.encoded : inputBody.encode}
                    onChange={onInputChange}
                    name="encode"
                />
            </Grid>

        </Grid>
    );

    if (loading) {
        form = <Spinner/>
    }

    return (
        <>
            {form}
        </>
    );
};

export default MainPage;